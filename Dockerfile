FROM      python:3.6
WORKDIR   /app
COPY      payment.py payment.ini rabbitmq.py requirements.txt /app/
RUN       pip3 install -r requirements.txt
COPY        run.sh /
ENTRYPOINT  ["bash", "/run.sh" ]